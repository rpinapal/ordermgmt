package com.freeLance.ordermanagement.jpa.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;


@Entity(name = "ORDERS")
public class Order {
	
//	ORDER_ID INT PRIMARY KEY AUTO_INCREMENT,
//	ORDER_NAME VARCHAR(30) NOT NULL,
//	ORDER_STATUS VARCHAR(20) NOT NULL,
//	DESTINATION_ADD VARCHAR(150) NOT NULL,
//	RECIPIENT_CONTACT VARCHAR(20),
//	TRACKER_ID VARCHAR(15) NOT NULL DEFAULT '',
//	UNIQUE KEY TRCKER(TRACKER_ID)
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ORDER_ID")
	private Long orderId;
	
	@Column(name = "ORDER_NAME")
	private String orderName;
	
	@Column(name = "ORDER_STATUS")
	private String orderStatus;
	
	@Column(name = "DESTINATION_ADD")
	private String destinationAdd;
	
	@Column(name = "RECIPIENT_CONTACT")
	private String recipientContact;
	
	@Column(name = "TRACKER_ID", unique = true)
	private String trackerId;
	
	@Column(name = "CREATION_TIME")
	@CreationTimestamp
	private Timestamp creationTime;

	public Order() {
		
	}
	
	public Order(Long orderId, String orderName, String orderStatus, String destinationAdd, String recipientContact,
			String trackerId, Timestamp timeStamp) {
		super();
		this.orderId = orderId;
		this.orderName = orderName;
		this.orderStatus = orderStatus;
		this.destinationAdd = destinationAdd;
		this.recipientContact = recipientContact;
		this.trackerId = trackerId;
		this.creationTime = timeStamp;
	}
	
	public Timestamp getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getDestinationAdd() {
		return destinationAdd;
	}

	public void setDestinationAdd(String destinationAdd) {
		this.destinationAdd = destinationAdd;
	}

	public String getRecipientContact() {
		return recipientContact;
	}

	public void setRecipientContact(String recipientContact) {
		this.recipientContact = recipientContact;
	}

	public String getTrackerId() {
		return trackerId;
	}

	public void setTrackerId(String trackerId) {
		this.trackerId = trackerId;
	}

	
}
