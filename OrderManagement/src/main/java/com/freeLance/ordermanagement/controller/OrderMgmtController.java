package com.freeLance.ordermanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freeLance.ordermanagement.jpa.model.Order;
import com.freeLance.ordermanagement.service.OrderService;

@Controller
public class OrderMgmtController {

	@Autowired
	private OrderService service;
	
	@GetMapping("/")
    public String home() {
		return "/admin";
    }
	
	@GetMapping("/login")
    public String login() {
        return "/login";
    }
	
	@GetMapping("/CreateOrder")
	public String showRegistration(Order order) {
		return "/CreateOrder";
	}
	
	@GetMapping("/orderDetail")
	public String showCreatedOrder(Order order) {
		return "/OrderDetail";
	}
	
	@PostMapping("/order")
	public String createOrder(Order order) {
		service.createOrder(order);
		return "/OrderDetail";
	}
	
	@GetMapping("/updateOrder")
	public String updateOrder(@RequestParam Long id, Model model) {
		Order order = service.getOrderById(id);
		model.addAttribute("order",order);
		return "/CreateOrder";
	}
	
	@GetMapping("/deleteOrder")
	public String deleteOrder(@RequestParam Long id) {
		service.deleteOrder(id);
		return "redirect:/order/1?sortField=creationTime&sortDir=desc";
	}
	
	
	@GetMapping("/order/{pageNo}")
    public String getOrders(@PathVariable(value = "pageNo") int pageNo,@RequestParam("sortField") String sortField,
    	    @RequestParam("sortDir") String sortDir,Model model) {
		int pageSize = 5;
		Page <Order> page = service.findPaginated(pageNo, pageSize,sortField,sortDir);
		List<Order> orders = page.getContent();
		model.addAttribute("currentPage", pageNo);
	    model.addAttribute("totalPages", page.getTotalPages());
	    model.addAttribute("totalItems", page.getTotalElements());
	    model.addAttribute("orders", orders);
	    model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		return "/ViewOrders";
    }
	
	@GetMapping("/trackOrder")
	public String showTracker(String trackerId) {
		return "/Tracker";
	}
	
	@GetMapping("/tracker")
	public String showTrackerResult(@RequestParam(value = "trackerId" ) String trackerId, Model model) {
		Order order = service.getOrderByTrackingId(trackerId);
		model.addAttribute("order", order);
		return "/Tracker";
	}

}
